<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('frontend.index');
});

Route::get('/cart','CartController@index')->name('cart.index');
Auth::routes();

Route::group(['middleware'=>['auth','user']], function (){
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::group(['middleware'=>['auth','admin']], function (){
    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    });

    Route::get('/registered-user','Admin\UserController@registered');
    Route::get('role-edit/{id}','Admin\UserController@edit');
    Route::put('role-update/{id}','Admin\UserController@update');
});

Route::group(['middleware'=>['auth','vendor']], function (){
    Route::get('/vendor-dashboard', function () {
        return view('vendor.dashboard');
    });
});

<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'tshirt-blue',
            'slug'=>'tshirt',
            'details'=> 'nice blue cotton',
            'price'=> 5456,
            'description'=>'ffhgfg ghghg jkghrfyu gdgdg',

                    ]);

        Product::create([
            'name' => 'tshirt-red',
            'slug'=>'tshirtred',
            'details'=> 'nice blue cotton',
            'price'=> 5456,
            'description'=>'ffhgfg ghghg jkghrfyu gdgdg',

        ]);

        Product::create([
            'name' => 'tshirt-green',
            'slug'=>'tshirtg',
            'details'=> 'nice blue cotton',
            'price'=> 5456,
            'description'=>'ffhgfg ghghg jkghrfyu gdgdg',

        ]);

        Product::create([
            'name' => 'tshirt-black',
            'slug'=>'tshirtblack',
            'details'=> 'nice blue cotton',
            'price'=> 5456,
            'description'=>'ffhgfg ghghg jkghrfyu gdgdg',

        ]);

        Product::create([
            'name' => 'tshirt-yellow',
            'slug'=>'tshirtyellow',
            'details'=> 'nice blue cotton',
            'price'=> 5456,
            'description'=>'ffhgfg ghghg jkghrfyu gdgdg',

        ]);

    }
}
